use clink::include;
include!{"tests/basic.h"}

// These are not defined in any of the header files apparently
type RsizeT = ();
type SizeT = ();
type WcharT = ();
type WintT = ();
type FILE = i32;
type FposT = ();
type ErrnoT = ();
type VaList = i32;

#[test]
fn test_basic() {
    unsafe {
        printf("Hello, World\nThis is a natural C header include!\n\0".as_ptr() as _);
        exit(0)
    }
}