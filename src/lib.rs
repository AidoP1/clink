use proc_macro::TokenStream;
use syn::{parse_macro_input, parse::{Parse, ParseStream}, Token};
use quote::quote;

use clang;

use syn::{
    Ident,
    Type,
    TypePtr
};

#[derive(Debug, Clone)]
enum IncludePath {
    Relative(String),
    System(String)
}
impl IncludePath {
    fn path(&self) -> &str {
        match self {
            Self::Relative(path) => path,
            Self::System(path) => path
        }
    }
}
impl Parse for IncludePath {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        if input.peek(Ident) {
            let system_ident = input.parse::<Ident>()?;
            if &format!("{}", system_ident) != "System" {
                return Err(input.error("Expected `System::`"))
            }
            let _ = input.parse::<Token![::]>()?;
            Ok(Self::System(
                input.parse::<syn::LitStr>()?.value()
            ))
        } else if input.peek(syn::LitStr) {
            Ok(Self::Relative(
                input.parse::<syn::LitStr>()?.value()
            ))
        } else {
            Err(input.error("Expected a `\"path.h\"` or `System::\"path.h\"`"))
        }
    }
}

use std::collections::HashSet;
#[proc_macro]
pub fn include(tokens: TokenStream) -> TokenStream {
    let span = proc_macro2::Span::call_site();

    let mut symbols = HashSet::new();

    // Get the PATH
    let path_var = std::env::var("PATH").expect("Unable to read PATH");

    let clang = &clang::Clang::new().expect("Unable start libclang");
    let index = clang::Index::new(clang, false, false);

    let path = parse_macro_input!(tokens as IncludePath);
    let path = path.path();

    let ast = index.parser(path).parse().expect("Unable to parse C file");
    let entities = ast.get_entity().get_children();

    let mut tokens = proc_macro2::TokenStream::new();

    for entity in entities {
        use clang::EntityKind::*;
        match entity.get_kind() {
            StructDecl => {
                let span = proc_macro2::Span::call_site();
                let name = rust_struct_name(&if let Some(name) = entity.get_name() { name } else { continue });
                if entity.get_name().expect("Struct must have a name").starts_with("_") || !symbols.insert(name.clone()) { continue }

                let struct_name = Ident::new(&name, span);
            
                let fields = entity.get_children();
                let field_names = fields.iter().map(|field| Ident::new(&rust_field_name(&field.get_name().unwrap()), span));
                let field_types = fields.iter().filter_map(|field| {
                    match field.get_kind() {
                        clang::EntityKind::FieldDecl => Some(rust_type(field.get_type().unwrap())),
                        clang::EntityKind::StructDecl => {
                            None
                        }
                        _ => panic!("Unexpected {:?}", field)
                    }
                });
            
                let t = quote!{
                    #[repr(C)]
                    struct #struct_name {
                        #(
                            #field_names: #field_types,
                        )*
                    }
                };
                tokens.extend(t)
            }
            TypedefDecl => {
                let name = rust_struct_name(&entity.get_name().expect("Typedef must have a name"));
                if entity.get_name().expect("Typedef must have a name").starts_with("_") || !symbols.insert(name.clone()) { continue }

                let type_name = Ident::new(&name, span);
                let type_target = rust_type(entity.get_typedef_underlying_type().unwrap());
                if !symbols.contains(&name) {
                    tokens.extend(quote!{
                        type #type_name = #type_target;
                    })
                }
            }
            FunctionDecl => {
                let name = &rust_field_name(&entity.get_name().unwrap());
                if entity.get_name().expect("Function must have a name").starts_with("_") || !symbols.insert(name.clone()) { continue }

                let fn_name = Ident::new(name, span);
                let fn_return = rust_type(entity.get_type().unwrap().get_result_type().unwrap());
                let arguments = entity.get_arguments().unwrap_or(Vec::new());
                let argument_names = arguments.iter().map(|argument| if let Some(name) = argument.get_name() {
                    let name = Ident::new(&rust_field_name(&name), span);
                    quote!{#name}
                } else {
                    quote!{var_args}
                });
                let argument_types = arguments.iter().map(|argument|  if let Some(_) = argument.get_name() {
                    let arg_type = rust_type(argument.get_type().unwrap());
                    quote!{#arg_type}
                } else {
                    quote!{...}
                });
                tokens.extend(quote!{
                    extern "C" {
                        fn #fn_name(#(#argument_names: #argument_types,)*) -> #fn_return;
                    }
                })
            }
            VarDecl => {
                let name = &rust_field_name(&entity.get_name().unwrap());
                if name.starts_with("_") || !symbols.insert(name.clone()) { continue }

                let var_name = Ident::new(name, span);
                let var_type = rust_type(entity.get_type().unwrap());
                tokens.extend(quote!{
                    extern "C" {
                        static mut #var_name: #var_type;
                    }
                })
            }
            _ => panic!("Unknown entity `{:?}`", entity)
        }
    }

    TokenStream::from(tokens)
}

fn rust_struct_name(c_name: &str) -> String {
    let mut cap_next = true;
    let mut new_str = String::new();
    for character in c_name.chars() {
        if character == '_' {
            cap_next = true;
        } else {
            if cap_next {
                new_str.push_str(&character.to_uppercase().to_string())
            } else {
                new_str.push(character)
            }
            cap_next = false
        }
    }

    new_str
}

fn rust_field_name(c_name: &str) -> String {
    let mut new_str = String::new();
    for character in c_name.chars() {
        if character.is_uppercase() {
            let mut substr = character.to_lowercase().to_string();
            if new_str.len() > 0 {
                substr.insert(0, '_');
            }
            new_str.push_str(&substr)
        } else {
            new_str.push(character)
        }
    }

    new_str
}

fn rust_type(c_type: clang::Type) -> syn::Type {
    let span = proc_macro2::Span::call_site();

    use clang::TypeKind::*;
    match c_type.get_kind() {
        Void => {
            Type::Verbatim(quote!{std::ffi::c_void})
        }
        Pointer => {
            Type::Ptr(TypePtr {
                star_token: Token![*](span),
                const_token: None,
                mutability: if c_type.is_const_qualified() { None } else { Some(Token![mut](span)) },
                elem: Box::new(rust_type(c_type.get_pointee_type().expect("Type is a pointer to nothing!")))
            })
        }
        Elaborated => {
            rust_type(c_type.get_elaborated_type().unwrap())
        }
        Record => {
            let ident = Ident::new(&rust_struct_name(&c_type.get_display_name().split_ascii_whitespace().nth(1).unwrap()), span);
            Type::Verbatim(quote!{#ident})
        }
        Typedef => {
            let ident = Ident::new(&rust_struct_name(&c_type.get_typedef_name().unwrap()), span);
            Type::Verbatim(quote!{#ident})
        }
        _ if c_type.is_integer() => {
            let ident = Ident::new(&format!("{}{}", if c_type.is_signed_integer() { "i" } else { "u" } , c_type.get_sizeof().unwrap() * 8), span);
            Type::Verbatim(quote!{#ident})
        }
        Double => {
            Type::Verbatim(quote!{f64})
        }
        _ => panic!("Unknown type `{:?}`", c_type)
    }
}